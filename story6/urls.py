from .views import *
from django.shortcuts import render
from django.urls import path

app_name = 'story6'
urlpatterns = [
    path('', index, name='index'),
    path('add_status/', add_status, name='add_status')
]
